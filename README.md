# InitRecheche TP1 et TP2 ray tracing
Maxime Delsart, M1 I2L

TP1 fait en partie avec Félicien Taverne

# TP2 - Exercice 5

## Méthode CURLESS
Sans O2, durée : 3 min 25s 

Avec O2, durée : 20 sec

## Méthode MOLLER
Sans O2, durée : 3 min 39s

Avec O2, durée : 16 sec 