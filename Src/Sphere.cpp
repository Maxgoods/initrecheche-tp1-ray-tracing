#include "Sphere.hpp"
#include <cmath>
#include <tuple>

// Delsart Maxime
// Fait en binome avec Félicien Taverne le 18 et 19 février

double SP_EPSILON = 0.0001;

Sphere::Sphere() : Objet(){
  rayon = 1.0;
}


Sphere::Sphere(float xc, float yc, float zc, float r, Materiau m) : Objet(m), centre(xc, yc, zc) {
  rayon = r;
}

Sphere::~Sphere(){}


// ligne à rajouter dans le .hpp : tuple<double, double> calcIntersection(const Rayon &r);
// Cette fonction permet de calculer delta et le point d'intersection
// et permet d'eviter la duplication de code dans intersecte et coupe
std::tuple<double, double>
    Sphere::calcIntersection(const Rayon& r) {
    // Origine du rayon
    double xA = r.origine.X; double yA = r.origine.Y; double zA = r.origine.Z;

    // Direction du rayon
    double xB = r.direction.dx; double yB = r.direction.dy; double zB = r.direction.dz;

    // Centre de la sphere
    double xC = this->centre.X; double yC = this->centre.Y; double zC = this->centre.Z;

    double xOc = xA - xC; double yOc = yA - yC; double zOc = zA - zC;

    double a = pow((xB), 2) + pow((yB), 2) + pow((zB), 2);
    double b = 2 * ((xB * xOc) + (yB * yOc) + (zB * zOc));
    double c = (pow(xA, 2) - (2 * xA * xC) + pow(xC, 2))
                + (pow(yA, 2) - (2 * yA *yC) + pow(yC, 2))
                + (pow(zA, 2) - (2 * zA * zC) + pow(zC, 2))
                - pow(this->rayon, 2);

    double delta = pow(b,2) - (4 * a * c);

    double minD;
    if (delta < 0){
        // do nothing
    } else if (delta == 0){
        minD = -b / ( 2 * a);
    } else {
        double x1 = (-b - sqrt(delta)) / (2 * a);
        double x2 = (-b + sqrt(delta)) / (2 * a);
        minD = std::min(x1,x2);
    }

    return std::make_tuple(delta, minD);
}

bool Sphere::intersecte(const Rayon& r, Intersection& inter){

    double delta, minD;

    tie(delta, minD) = this->calcIntersection(r);

    if (minD < SP_EPSILON || delta < 0) {
        return false;
    }

    double finalX = r.direction.dx * minD + r.origine.X;
    double finalY = r.direction.dy * minD + r.origine.Y;
    double finalZ = r.direction.dz * minD + r.origine.Z;

    inter = Intersection(Point(finalX, finalY, finalZ), this, minD);
    return true;
}

bool Sphere::coupe(const Rayon& r){
    double delta, minD;

    tie(delta, minD) = this->calcIntersection(r);

    if (minD < SP_EPSILON) {
        return false;
    }

  return true;
}

ostream& operator<<(ostream & sortie, Sphere & s){
  // affichage de l'équation de la sphère
  sortie << "Sphere : de rayon " << s.rayon << ", de centre ";
  sortie << s.centre;
  // affichage du matériau de la sphère
  Objet *po = &s;
  sortie << *po << flush;
  return sortie;
}

Vecteur Sphere::getNormale(const Point &p){
  // la normale à la sphère au point P est égale
  // au vecteur CP, avec C le centre de la sphère
  Vecteur n(p.X-centre.X,p.Y-centre.Y, p.Z-centre.Z);
  n.normaliser();
  return n;
}


void Sphere::affiche(ostream& out) {
  out << "Sphere : de rayon " << rayon << ", de centre ";
  out << centre;
  Objet::affiche(out);
}

