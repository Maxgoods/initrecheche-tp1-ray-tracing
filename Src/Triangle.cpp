#include <tuple>
#include "Triangle.hpp"
#include "Plan.hpp"

double TRIANGLE_EPSILON = 0.000001;

Triangle::Triangle() : Objet(){
  s[0].set(-1, 0, -1);
  s[1].set(1, 0, -1);
  s[2].set(0, 0, 1);
  n.set(0, 0, 1);
}

Triangle::Triangle(const Point p[3], Materiau m) : Objet(m) {
  for(int i=0; i<3; i++)
    s[i] = p[i];
  // calcul de la normale à partir du produit vectoriel AB^AC
  // cette normale doit ensuite être normalisée...
    for(int i=0; i<3; i++)
        s[i] = p[i];
    // calcul de la normale à partir du produit vectoriel AB^AC
    Vecteur AB = Vecteur(s[0], s[1]);
    Vecteur AC = Vecteur(s[0], s[2]);

    n = n.cross(AB, AC);
    // cette normale doit ensuite être normalisée...
    n.normaliser();
}

Triangle::~Triangle(){}

// Cette fonction permet d'eviter la duplication de code dans intersecte et coupe
#ifdef CURLESS
std::tuple<bool, float, Point>
Triangle::calcIntersection(const Rayon& r) {
    float d = this->n.dx * this->s[0].X + this->n.dy * this->s[0].Y + this->n.dz * this->s[0].Z;
    float dOrigin = this->n.dx * r.origine.X + this->n.dy * r.origine.Y + this->n.dz * r.origine.Z;

    float numerateurT = d - dOrigin;
    float denominateurT = this->n * r.direction;

    // division par 0 impossible + doc 1 : if n * d = 0, then d is parallel
    if(denominateurT == 0){
        return std::make_tuple(false, 0, Point());
    }
    float t = numerateurT / denominateurT;

    if(t < TRIANGLE_EPSILON) return std::make_tuple(false, 0, Point());

    // Create vector Q
    float x_Q = r.origine.X + t * r.direction.dx;
    float y_Q = r.origine.Y + t * r.direction.dy;
    float z_Q = r.origine.Z + t * r.direction.dz;
    Vecteur Q = Vecteur(x_Q,y_Q,z_Q);

    // Check ((B - A) * (Q - A)) * n > 0
    float x_BA = this->s[1].X - this->s[0].X;
    float y_BA = this->s[1].Y - this->s[0].Y;
    float z_BA = this->s[1].Z - this->s[0].Z;

    float x_QA = Q.dx - this->s[0].X;
    float y_QA = Q.dy - this->s[0].Y;
    float z_QA = Q.dz - this->s[0].Z;

    Vecteur v_BA = Vecteur(x_BA,y_BA,z_BA);
    Vecteur v_QA = Vecteur(x_QA,y_QA,z_QA);
    Vecteur v_BA_QA = Vecteur(v_BA.cross(v_BA, v_QA));
    float p_BA_QA = v_BA_QA.operator * (this->n);
    if(p_BA_QA < 0) return std::make_tuple(false, 0, Point());

    // Check ((C - B) * (Q - B)) * n > 0
    float x_CB = this->s[2].X - this->s[1].X;
    float y_CB = this->s[2].Y - this->s[1].Y;
    float z_CB = this->s[2].Z - this->s[1].Z;

    float x_QB = Q.dx - this->s[1].X;
    float y_QB = Q.dy - this->s[1].Y;
    float z_QB = Q.dz - this->s[1].Z;

    Vecteur v_CB = Vecteur(x_CB,y_CB,z_CB);
    Vecteur v_QB = Vecteur(x_QB,y_QB,z_QB);
    Vecteur v_CB_QB = Vecteur(v_CB.cross(v_CB, v_QB));
    float p_CB_QB = v_CB_QB.operator * (this->n);
    if(p_CB_QB < 0) return std::make_tuple(false, 0, Point());

    // Check ((A - C) * (Q - C)) * n > 0
    float x_AC = this->s[0].X - this->s[2].X;
    float y_AC = this->s[0].Y - this->s[2].Y;
    float z_AC = this->s[0].Z - this->s[2].Z;

    float x_QC = Q.dx - this->s[2].X;
    float y_QC = Q.dy - this->s[2].Y;
    float z_QC = Q.dz - this->s[2].Z;

    Vecteur v_AC = Vecteur(x_AC,y_AC,z_AC);
    Vecteur v_QC = Vecteur(x_QC,y_QC,z_QC);
    Vecteur v_AC_QC = Vecteur(v_AC.cross(v_AC, v_QC));
    float p_AC_QC = v_AC_QC.operator * (this->n);
    if(p_AC_QC < 0) return std::make_tuple(false, 0, Point());


    Point interPoint = Point(Q.dx, Q.dy, Q.dz);

    return std::make_tuple(true, t, interPoint);
}
#endif

#ifdef MOLLER
std::tuple<bool, float, Point>
Triangle::calcIntersection(const Rayon& r) {
    float x_EDGE_1 = this->s[1].X - this->s[0].X;
    float y_EDGE_1 = this->s[1].Y - this->s[0].Y;
    float z_EDGE_1 = this->s[1].Z - this->s[0].Z;

    Vecteur EDGE_1 = Vecteur(x_EDGE_1, y_EDGE_1, z_EDGE_1);

    float x_EDGE_2 = this->s[2].X - this->s[0].X;
    float y_EDGE_2 = this->s[2].Y - this->s[0].Y;
    float z_EDGE_2 = this->s[2].Z - this->s[0].Z;

    Vecteur EDGE_2 = Vecteur(x_EDGE_2, y_EDGE_2, z_EDGE_2);

    Vecteur pvec = r.direction.cross(r.direction, EDGE_2);
    float det = EDGE_1 * pvec;

    if(det > -TRIANGLE_EPSILON && det < TRIANGLE_EPSILON) {
        return std::make_tuple(false, 0, Point());
    }

    float inv_det = 1.0 / det;
    Vecteur orig = Vecteur(r.origine.X, r.origine.Y, r.origine.Z);
    Vecteur tvec = orig - Vecteur(this->s[0].X, this->s[0].Y, this->s[0].Z);

    float u = tvec * pvec * inv_det;
    if(u < 0.0 || u > 1.0) return std::make_tuple(false, 0, Point());

    Vecteur qvec = tvec.cross(tvec, EDGE_1);
    float v = qvec * r.direction * inv_det;

    if(v < 0.0 || u + v > 1.0) return std::make_tuple(false, 0, Point());

    float ot = EDGE_2 * qvec * inv_det;

    if(ot <= TRIANGLE_EPSILON){
        return std::make_tuple(false, 0, Point());
    }

    float x_Q = r.origine.X + ot * r.direction.dx;
    float y_Q = r.origine.Y + ot * r.direction.dy;
    float z_Q = r.origine.Z + ot * r.direction.dz;

    Point interPoint = Point(x_Q, y_Q, z_Q);

    return std::make_tuple(true, ot, interPoint);
}
#endif

#ifdef CURLESS
bool Triangle::intersecte(const Rayon& r, Intersection& inter){
    bool success;
    float t;
    Point interPoint;

    tie(success, t, interPoint) = this->calcIntersection(r);

    if (!success) return false;

    inter = Intersection(interPoint, this, t);

    return true;
}
#endif

#ifdef MOLLER
bool Triangle::intersecte(const Rayon& r, Intersection& inter){
    bool success;
    float ot;
    Point interPoint;

    tie(success, ot, interPoint) = this->calcIntersection(r);

    if (!success) return false;

    inter = Intersection(interPoint, this, ot);

    return true;
}
#endif

#ifdef CURLESS
bool Triangle::coupe(const Rayon& r){
    bool success;
    float t;
    Point interPoint;

    tie(success, t, interPoint) = this->calcIntersection(r);

    if (!success) return false;

    return true;
}
#endif

#ifdef MOLLER
bool Triangle::coupe(const Rayon& r){
    bool success;
    float ot;
    Point interPoint;

    tie(success, ot, interPoint) = this->calcIntersection(r);

    if (!success) return false;

    return true;
}
#endif


Vecteur Triangle::getNormale(const Point &p){
  return n;
}

ostream& operator<<(ostream & sortie, Triangle & t){
  sortie << "triangle : ";
  for(int i=0; i<3; i++)
    sortie << t.s[i] << " - ";
  sortie << endl;

  return sortie;
    
}

void Triangle::affiche(ostream& out){
  out << "triangle : ";
  for(int i=0; i<3; i++)
    out << s[i] << " - ";
  out << endl;
}

